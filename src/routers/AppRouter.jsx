import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom"

import Navbar from "../components/Navbar"
import PSoftwareE2E from "../components/pages/soluciones/PSoftwareE2E"
import PPerformance from "../components/pages/soluciones/PPerformance"
import PAplicacionesMoviles from "../components/pages/soluciones/PAplicacionesMoviles"
import PAccesibilidad from "../components/pages/soluciones/PAccesibilidad"
import PAutomatizacion from "../components/pages/soluciones/PAutomatizacion"
import DHerramientasPruebas from "../components/pages/soluciones/DHerramientasPruebas"
import Main from "../components/pages/Main"
import Contacto from '../components/pages/Contacto'
import Footer from "../components/Footer"


const AppRouter = () => {
    return (
        <>
            <BrowserRouter>
                <Navbar />
                <Routes>
                    <Route path="/" element={ <Main /> }></Route>
                    <Route path="/soluciones">
                        <Route path="/soluciones/pruebas-de-software-end-to-end" element={<PSoftwareE2E />} />
                        <Route path="/soluciones/pruebas-de-performance" element={< PPerformance />} />
                        <Route path="/soluciones/pruebas-en-aplicaciones-moviles" element={< PAplicacionesMoviles />} />
                        <Route path="/soluciones/pruebas-de-accesibilidad" element={< PAccesibilidad />} />
                        <Route path="/soluciones/automatizacion-de-pruebas" element={< PAutomatizacion />} />
                        <Route path="/soluciones/desarrollo-de-herramientas-de-pruebas" element={< DHerramientasPruebas />} />
                    </Route>
                    <Route path="*" element={ <Navigate to="/" /> }/>
                </Routes>
            </BrowserRouter>
            <Contacto />
            <Footer />
        </>
    )
}

export default AppRouter