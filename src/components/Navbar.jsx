import React, { useState } from 'react'
import { Link } from 'react-scroll'
import { NavLink } from 'react-router-dom'
import Dropdown from './Dropdown'

const Navbar = () => {
    const [click, setClick] = useState(false)
    const [dropdown, setDropdown] = useState(false)

    const handleClick = () => setClick(!click)
    const closeMobileMenu = () => setClick(false)

    const onMouseEnter = () => {
        if (window.innerWidth <= 768) {
            setDropdown(false)
        } else {
            setDropdown(true)
        }
    }

    const onMouseLeave = () => {
        if (window.innerWidth <= 768) {
            setDropdown(false)
        } else {
            setDropdown(false)
        }
    }

    const initialStyle = "md:inline-flex md:flex-grow md:w-auto"
    const ulStyle = click ? initialStyle : initialStyle + " hidden"
    
    return (
        <>
            <nav className="fixed w-full z-10 shadow-md md:flex text-sm font-medium p-3 bg-neutral-50 flex-wrap justify-center">
                <div className='max-w-7xl mx-auto md:flex md:flex-row flex-col justify-between items-center'>
                    <div className='flex'>
                        <NavLink to='/' className='inline-flex cursor-pointer mr-4 p-2 items-center'>
                            <span className='text-3xl text-slate-900 font-bold tracking-wide'>Binareon</span>
                            <i className='fab fa-firstdraft text-3xl ml-2'></i>
                        </NavLink   >
                        <button className='text-slate-900 text-xl p-3 ml-auto hover:bg-slate-200 cursor-pointer rounded md:hidden' onClick={handleClick}>
                            <i className={click ? 'fas fa-times' : 'fas fa-bars'}></i>
                        </button>
                    </div>
                    <ul className={ulStyle}>
                        <div className='md:flex md:flex-row flex flex-col'>
                            <li>
                                <Link to='home' smooth={true} duration={1000} className='inline-flex w-full cursor-pointer md:w-auto px-6 py-2 rounded text-slate-900 hover:bg-slate-300 hover:transition-all hover:duration-500' onClick={closeMobileMenu}>
                                    Inicio
                                </Link>
                            </li>
                            <li onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
                                <Link to='solutions' smooth={true} duration={1000} className='inline-flex w-full cursor-pointer md:w-auto px-6 py-2 rounded text-slate-900 hover:bg-slate-300 hover:transition-all hover:duration-500' onClick={closeMobileMenu}>
                                    Soluciones
                                    <div className='hidden md:block'>
                                        <i className='fas fa-caret-down ml-2'></i>
                                    </div>
                                </Link>
                                {dropdown && <Dropdown />}
                            </li>
                            <li>
                                <Link to='about-us' smooth={true} duration={1000} className='inline-flex w-full cursor-pointer md:w-auto px-6 py-2 rounded text-slate-900 hover:bg-slate-300 hover:transition-all hover:duration-500' onClick={closeMobileMenu}>
                                    Nosotros
                                </Link>
                            </li>
                            <li>
                                <Link to='clients' smooth={true} duration={1000} className='inline-flex w-full cursor-pointer md:w-auto px-6 py-2 rounded text-slate-900 hover:bg-slate-300 hover:transition-all hover:duration-500' onClick={closeMobileMenu}>
                                    Clientes
                                </Link>
                            </li>
                            <li>
                                <Link to='contact' smooth={true} duration={1000} className='inline-flex w-full cursor-pointer md:w-auto px-6 py-2 rounded text-slate-900 hover:bg-slate-300 hover:transition-all hover:duration-500' onClick={closeMobileMenu}>
                                    Contáctenos
                                </Link>
                            </li>
                        </div>
                    </ul>
                </div>
            </nav>
        </>
    )
}

export default Navbar