import React from 'react'

const Footer = () => {
    return (
        <>
            <footer className='pt-10 bg-slate-900 w-full text-neutral-50'>
                <div className='max-w-7xl mx-auto px-10'>
                    <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-2'>
                        <div className='mb-5'>
                            <h2 className='text-sky-500 font-bold text-lg'>Binareon</h2>
                            <p className='pt-10 text-sm font-bold'>Somos líderes en soluciones de Software</p>
                            <p className='text-sm text-neutral-400'>Siempre la calidad para el cliente</p>
                        </div>
                        <div className='mb-5'>
                            <h2 className='text-sky-500 font-bold text-lg'>Contacto</h2>
                            <p className='pt-10 pb-1 text-sm font-bold uppercase'>Perú</p>
                            <p className='py-1 pt-2 text-sm text-neutral-400'>Callao, Lima</p>
                            <p className='py-1 text-sm text-neutral-400'>Pj. Victor R. Haya de la Torre Mz. E Lt. 19</p>
                            <p className='py-1 text-sm text-neutral-400'>Albino Herrera Etapa 1</p>
                        </div>
                        <div className='mb-5'>
                            <h2 className='text-sky-500 font-bold text-lg'>Mas Información</h2>
                            <p className='pt-10 pb-1 text-sm text-neutral-400'>Siempre la calidad para el cliente</p>
                            <p className='py-1 text-sm text-neutral-400'>Siempre la calidad para el cliente</p>
                            <p className='py-1 text-sm text-neutral-400'>Siempre la calidad para el cliente</p>
                            <p className='py-1 text-sm text-neutral-400'>Siempre la calidad para el cliente</p>
                        </div>
                        <div className='mb-5'>
                            <h2 className='text-sky-500 font-bold text-lg'>Nuestras Redes</h2>
                        </div>

                    </div>
                </div>
                <div className='w-full bg-gray-900'>
                    <hr className='bg-sky-600 h-1 bg-gradient-to-r from-zinc-900 to-sky-600' />
                    <div className='text-center py-3 px-10'>
                        <span className='text-sm font-light'>Binareon &copy; 2022. Todos los derechos reservados.</span>
                    </div>
                </div>
            </footer>
        </>
    )
}

export default Footer