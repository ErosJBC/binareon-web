import React, { useState} from 'react'
import { MenuItems } from './MenuItems.js'
import { Link } from 'react-router-dom'

const Dropdown = () => {
    const [click, setClick] = useState(false)
    const handleClick = () => setClick(!click)

    return (
        <>
            <ul onClick={handleClick} className={click ? 'absolute w-44 hidden' : 'absolute w-44'} >
                {MenuItems.map((item, index) => {
                    return (
                        <li key={index}>
                            <Link className='bg-neutral-50 text-slate-900 text-md block w-full h-full px-5 py-2 hover:bg-slate-800 hover:text-white hover:rounded' to={item.path} onClick={() => setClick(false)}>{item.title}</Link>
                        </li>
                    )
                })}
            </ul>
        </>
    )
}

export default Dropdown