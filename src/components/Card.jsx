import React, { Fragment } from 'react'
import { PropTypes } from 'prop-types'

const Card = ({ title, description, icon_card, icon_color, bool_icon }) => {
    const style_icon = bool_icon === true ? 'pt-6 text-4xl ' + icon_color : 'hidden'
    const style_number = bool_icon === true ? 'hidden': 'text-4xl font-semibold pt-6'
    return (
        <Fragment>
            <div className='bg-white container max-w-md h-full mx-auto transition-all duration-500 rounded-md shadow-lg md:max-w-2xl hover:shadow-2xl hover:shadow-gray-300 hover:transition-all hover:duration-500 hover:-translate-y-1'>
                <div className='flex flex-col px-2 sm:px-1 md:px-10 mx-10 md:mx-0 md:pb-8 pb-4 pt-4 cursor-pointer'>
                    <div className={style_icon}>
                        <i className={icon_card}></i>
                    </div>
                    <div className={style_number}>
                        <span className='text-sky-500'>{icon_card}</span>
                    </div>
                    <div>
                        <h2 className='text-slate-700 font-semibold text-center md:text-left text-xl pt-4'>{title}</h2>
                    </div>
                    <div>
                        <p className='text-center leading-7 md:text-justify text-sm text-slate-900 sm:text-left py-4'>{description}</p>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

Card.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    icon_card: PropTypes.string,
    icon_color: PropTypes.string,
    bool_icon: PropTypes.bool
}

export default Card