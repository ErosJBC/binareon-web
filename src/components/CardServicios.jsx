import React from 'react'
import { PropTypes } from 'prop-types'

const CardServicios = ({ title, description, indicator_color}) => {

    return (
        <>
            <div className='bg-white container max-w-sm mx-auto transition-all duration-500 rounded-md shadow-md md:max-w-xl hover:shadow-gray-300'>
                <div className='flex flex-col px-2 sm:px-1 md:px-10 mx-10 md:mx-0 md:pb-8 pb-4 pt-4 cursor-pointer'>
                    <div>
                        <h2 className='text-slate-700 font-semibold text-center md:text-left text-xl pt-4'><span className={indicator_color}></span>{title}</h2>
                    </div>
                    <div>
                        <p className='text-center leading-7 md:text-justify text-sm text-slate-900 sm:text-left py-4'>{description}</p>
                    </div>
                </div>
            </div>
        </>
    )
}

CardServicios.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    indicator_color: PropTypes.string
}

export default CardServicios