
export const MenuItems = [
    {
        title: 'Pruebas de Software End-to-End',
        path: '/soluciones/pruebas-de-software-end-to-end'
    },
    {
        title: 'Pruebas de Performance',
        path: '/soluciones/pruebas-de-performance'
    },
    {
        title: 'Pruebas en Aplicaciones Móviles',
        path: '/soluciones/pruebas-en-aplicaciones-moviles'
    },
    {
        title: 'Pruebas de Accesibilidad',
        path: '/soluciones/pruebas-de-accesibilidad'
    },
    {
        title: 'Automatización de Pruebas',
        path: '/soluciones/automatizacion-de-pruebas'
    },
    {
        title: 'Desarrollo de Herramientas de Pruebas',
        path: '/soluciones/desarrollo-de-herramientas-de-pruebas'
    }
]