import React from 'react'
import Inicio from './Inicio'
import Soluciones from './Soluciones'
import Nosotros from './Nosotros'
import Clientes from './Clientes'
import { Link } from 'react-router-dom'

const Main = () => {
    return (
        <>
            <Inicio />
            <Soluciones />
            <Nosotros />
            <Clientes />
        </>
    )
}

export default Main