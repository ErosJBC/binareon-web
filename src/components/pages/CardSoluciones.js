
export const CardSoluciones = [
    {
        title: 'Pruebas de Software End-to-End',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: 'fas fa-regular fa-chart-line',
        icon_color: 'text-teal-400',
        bool_icon: true,
        path: '/soluciones/pruebas-de-software-end-to-end'
    },
    {
        title: 'Pruebas de Performance',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: 'fas fa-regular fa-gauge-high',
        icon_color: 'text-amber-300',
        bool_icon: true,
        path: '/soluciones/pruebas-de-performance'
    },
    {
        title: 'Pruebas en Aplicaciones Móviles',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: 'fas fa-regular fa-mobile-screen',
        icon_color: 'text-blue-400',
        bool_icon: true,
        path: '/soluciones/pruebas-en-aplicaciones-moviles'
    },
    {
        title: 'Pruebas de Accesibilidad',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: 'fas fa-light fa-file-shield',
        icon_color: 'text-indigo-400',
        bool_icon: true,
        path: '/soluciones/pruebas-de-accesibilidad'
    },
    {
        title: 'Automatización de Pruebas',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: 'fas fa-regular fa-gears',
        icon_color: 'text-pink-400',
        bool_icon: true,
        path: '/soluciones/automatizacion-de-pruebas'
    },
    {
        title: 'Desarrollo de Herramientas de Pruebas',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: 'fas fa-regular fa-laptop-code',
        icon_color: 'text-cyan-400',
        bool_icon: true,
        path: '/soluciones/desarrollo-de-herramientas-de-pruebas'
    }
]