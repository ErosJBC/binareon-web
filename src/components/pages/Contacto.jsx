import React, { useRef, useState } from 'react'
import ReCAPTCHA from "react-google-recaptcha"

const Contacto = () => {

    const [captchaValido, setCaptchaValido] = useState(null)
    const [usuarioValido, setUsuarioValido] = useState(false)

    const captcha = useRef(null);

    const onChange = () => {
        if (captcha.current.getValue()) {
            console.log('El usuario no es un robot')
            setCaptchaValido(true)
        }
    }

    const submit = (e) => {
        e.preventDefault()
        if (captcha.current.getValue()) {
            console.log('El usuario no es un robot')
            setUsuarioValido(true)
            setCaptchaValido(true)
        } else {
            console.log('Por favor acepta el captcha')
            setUsuarioValido(false)
            setCaptchaValido(false)
        }
    }

    return (
        <>
            <section id='contact' className='lg:max-lg bg-slate-50 pt-28 pb-14'>
                <div className='max-w-7xl mx-auto'>
                    <div className='grid grid-cols-1 lg:grid-cols-2 md:mx-12 mx-12'>
                        <div className='md:pr-12'>
                            <h2 className='hover:text-blue-700 hover:transition-all pb-5 hover:duration-300 hover:ease-in text-blue-500 text-3xl font-semibold text-center'>Contáctenos</h2>
                            <hr className='block mx-auto bg-slate-500 opacity-50 h-0.5 w-20'/>
                            <p className='text-base text-slate-900 pt-6 leading-7 text-justify'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique, at libero alias eligendi facere perferendis. Minima blanditiis, asperiores error harum doloribus sit eaque illo accusamus ab labore saepe nisi nam?
                                Quae nesciunt ut sequi illo explicabo deleniti tenetur maiores eaque, aut doloribus eos, ex harum? Voluptate quae, quis, laudantium tempora harum officia exercitationem dolor sunt ea enim in itaque quos!</p>
                        </div>
                        <div className='pt-12 lg:pt-0'>
                            <div className='bg-white max-w-lg rounded-lg mx-auto shadow-md'>
                                <h2 className='text-center pb-6 pt-8 md:pt-14 text-2xl md:text-3xl text-slate-600'>Déjenos un mensaje</h2>
                                <hr className='block mx-auto bg-slate-500 opacity-50 h-0.5 w-44' />
                                <form className='grid grid-cols-1 gap-y-6 relative bg-white mx-w-3xl mx-auto pt-12 md:pb-6 px-8 sm:px-10 md:pt-12 md:px-12' action="" onSubmit={submit}>
                                    <div>
                                        <input className='block w-full ring-1 ring-gray-200 shadow-sm py-2 px-4 placeholder-gray-500 border-gray-300 rounded-md' type="text" name="" id="nombre" placeholder='Nombre' required />
                                    </div>
                                    <div>
                                        <input className='block w-full ring-1 ring-gray-200 shadow-sm py-2 px-4 placeholder-gray-500 border-gray-300 rounded-md' type="text" name="" id="nombre" placeholder='Organización' required />
                                    </div>
                                    <div>
                                        <input className='block w-full ring-1 ring-gray-200 shadow-sm py-2 px-4 placeholder-gray-500 border-gray-300 rounded-md' type="text" name="" id="nombre" placeholder='Cargo / Puesto' required />
                                    </div>
                                    <div>
                                        <input className='block w-full ring-1 ring-gray-200 shadow-sm py-2 px-4 placeholder-gray-500 border-gray-300 rounded-md' type="text" name="" id="nombre" placeholder='Correo' required />
                                    </div>
                                    <div>
                                        <input className='block w-full ring-1 ring-gray-200 shadow-sm py-2 px-4 placeholder-gray-500 border-gray-300 rounded-md' type="text" name="" id="nombre" placeholder='Celular' required />
                                    </div>
                                    <div>
                                        <textarea className='block w-full ring-1 ring-gray-200 shadow-sm h-28 py-3 px-4 placeholder-gray-500 border-gray-300 rounded-md' name="" id="nombre" placeholder='Escriba un mensaje...' required />
                                    </div>
                                    <div className='mx-auto'>
                                        <ReCAPTCHA ref={captcha} sitekey='6Ld7HmcfAAAAAEBi0ZZnb9_MmQTNeAkvJVyrAgsn' onChange={onChange} />
                                    </div>
                                    {captchaValido === false && <span className='text-red-800 text-center font-medium mx-20 p-1 rounded-sm border border-red-500 bg-rose-300 text-xs '>Captcha no validado</span>}
                                    <div className='pb-6'>
                                        <button type="submit" className=' w-full justify-center items-center py-4 px-6 border-transparent shadow-sm rounded-md bg-gradient-to-r from-teal-600 to-emerald-500 transition-all duration-500 text-neutral-50 text-md uppercase cursor-pointer hover:-translate-y-1 hover:shadow-xl hover:shadow-gray-300 hover:transition-all hover:duration-500'>Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Contacto