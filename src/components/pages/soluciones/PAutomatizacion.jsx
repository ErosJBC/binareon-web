import React from 'react'
import { CardPAutomatizacion } from './area-servicios/CardPAutomatizacion'
import AreaServicios from './AreaServicios'

const PAutomatizacion = () => {
  return (
    <>
      <section id='home' className="bg-slate-50 z-10 pt-28">
        <div className='max-w-7xl mx-auto grid grid-cols-1 md:grid-cols-2 pt-20'>
          <div className='mx-12 md:mx-12'>
            <h2 className='pb-5 text-slate-700 text-4xl font-normal text-left'>Automatización de Pruebas</h2>
            <p>Aumente el ROI de las pruebas, al tiempo que mejora la efectividad y la capacidad de mantenimiento</p>
            <p className='pt-5 text-slate-900 text-lg text-justify leading-9'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet at enim dicta perspiciatis quo doloribus error laudantium, dignissimos pariatur, molestiae quos asperiores natus esse sed excepturi consectetur recusandae aut!</p>
          </div>
        </div>
      </section>
      {CardPAutomatizacion.map((data, index) => {
        return (
          <AreaServicios key={index} ListCard={data.data}/>
        )
      })}
    </>
  )
}

export default PAutomatizacion