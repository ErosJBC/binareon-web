import React from 'react'
import { CardPAccesibilidad } from './area-servicios/CardPAccesibilidad'
import AreaServicios from './AreaServicios'

const PAccesibilidad = () => {
  return (
    <>
      <section id='home' className="bg-slate-50 z-10 pt-28">
        <div className='max-w-7xl mx-auto grid grid-cols-1 md:grid-cols-2 pt-20'>
          <div className='mx-12 md:mx-12'>
            <h2 className='pb-5 text-slate-700 text-4xl font-normal text-left'>Pruebas de Accesibilidad</h2>
            <p>Asegúrese que sus Aplicaciones Móviles y sitios web cumplan con los estándares de accesibilidad más exigentes, garantizando una excelente experiencia de usuario</p>
            <p className='pt-5 text-slate-900 text-lg text-justify leading-9'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet at enim dicta perspiciatis quo doloribus error laudantium, dignissimos pariatur, molestiae quos asperiores natus esse sed excepturi consectetur recusandae aut!</p>
          </div>
        </div>
      </section>
      {CardPAccesibilidad.map((data, index) => {
        return (
          <AreaServicios key={index} ListCard={data.data}/>
        )
      })}
    </>
  )
}

export default PAccesibilidad