import React from 'react'
import CardServicios from '../../CardServicios'

const AreaServicios = ({ ListCard }) => {

    return (
        <section className='bg-slate-50 z-10'>
            <div className='max-w-7xl mx-auto'>
                <div className='sm:mx-4 md:mx-6'>
                    <div className='pt-24 pb-12'>
                        <h2 className='hover:text-blue-700 hover:transition-all pb-4 hover:duration-300 hover:ease-in text-blue-500 text-3xl font-semibold text-center'>Área claves del servicio</h2>
                        <hr className='block mx-auto bg-slate-500 opacity-50 h-0.5 w-20' />
                    </div>
                    <div className='grid md:grid-cols-3 sm:grid-cols-2 sm:gap-x-6 sm:mx-8 md:mx-6 md:gap-x-8 mx-12 gap-10'>
                        {ListCard.map((item, index) => {
                            return (
                                <CardServicios key={index} title={item.title} description={item.description} indicator_color={item.indicator_color} />
                            )
                        })}
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AreaServicios