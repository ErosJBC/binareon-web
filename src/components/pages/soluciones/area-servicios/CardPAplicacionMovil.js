
export const CardPAplicacionMovil = [
    {
        data: [
            {
                title: 'Perfiles de Rendimiento',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-teal-400'
            },
            {
                title: 'Pruebas Móviles Continuas',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-amber-300'
            },
            {
                title: 'Pruebas de Aceptación',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-blue-400'
            },
            {
                title: 'Automatización de Pruebas Móviles',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-indigo-400'
            }
        ]
    }
]