
export const CardDHP = [
    {
        data: [
            {
                title: 'Herramientas de Automatización de Pruebas',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-teal-400'
            },
            {
                title: 'Herramientas de Pruebas de Rendimiento',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-amber-300'
            },
            {
                title: 'Implementación y soporte RPA',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-blue-400'
            },
            {
                title: 'Inteligencia Artificial',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-indigo-400'
            }
        ]
    }
]