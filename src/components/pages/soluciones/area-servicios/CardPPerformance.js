
export const CardPPerformance = [
    {
        data: [
            {
                title: 'Evaluación de Madurez',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-teal-400'
            },
            {
                title: 'Estrategia y ejecución',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-amber-300'
            },
            {
                title: 'Reportes y monitoreo',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-blue-400'
            },
            {
                title: 'Servicios DevOps',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-indigo-400'
            }
        ]
    }
]