
export const CardPSE2E = [
    {
        data: [
            {
                title: 'Pruebas Funcionales',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-teal-400'
            },
            {
                title: 'Pruebas de Carga',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-amber-300'
            },
            {
                title: 'Pruebas de Aplicaciones Móviles',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-blue-400'
            },
            {
                title: 'Pruebas de Automatización',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-indigo-400'
            },
            {
                title: 'Pruebas de Usabilidad',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-pink-400'
            },
            {
                title: 'Pruebas de Seguridad',
                description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
                indicator_color: 'text-cyan-400'
            }
        ]
    }
]