
export const CardNosotros = [
    {
        title: 'Un equipo comprometido',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: '01',
        icon_color: 'text-sky-500',
        bool_icon: false
    },
    {
        title: 'La mejor metodología',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: '02',
        icon_color: 'text-sky-500',
        bool_icon: false
    },
    {
        title: 'Herramientas adecuadas',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam non laboriosam eum perferendis iste eaque accusantium est! Debitis aspernatur quia minima asperiores unde velit, incidunt nam vero ipsam ut cumque!',
        icon_card: '03',
        icon_color: 'text-sky-500',
        bool_icon: false
    }
]