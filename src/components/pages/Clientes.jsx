import React from 'react'

const Clientes = () => {
    return (
        <>
            <section id='clients' className='lg:max-lg bg-slate-50 pt-28'>
                <div className='max-w-7xl mx-auto'>
                    <div className='grid grid-cols-1 lg:grid-cols-2 md:mx-12 mx-12'>
                        <div className='md:pr-10'>
                            <h2 className='hover:text-blue-700 text-center hover:transition-all pb-5 hover:duration-300 hover:ease-in text-blue-500 text-3xl font-semibold'>Clientes</h2>
                            <hr className='block mx-auto bg-slate-500 opacity-50 h-0.5 w-20'/>
                            <p className='text-md leading-7 pt-6 text-slate-900 text-justify'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique, at libero alias eligendi facere perferendis. Minima blanditiis, asperiores error harum doloribus sit eaque illo accusamus ab labore saepe nisi nam?
                                Quae nesciunt ut sequi illo explicabo deleniti tenetur maiores eaque, aut doloribus eos, ex harum? Voluptate quae, quis, laudantium tempora harum officia exercitationem dolor sunt ea enim in itaque quos!</p>
                        </div>
                        <div className='pl-0 lg:pl-10'>
                            <div className='pt-12 md:pt-8'>
                                <div className='pb-5'>
                                    <h2 className='text-gray-700 text-2xl font-semibold pb-4'>Creemos en la calidad</h2>
                                    <p className='text-base text-slate-900 leading-7 text-justify'>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugit ratione corrupti id nihil cumque, sunt accusamus, inventore temporibus omnis consectetur unde alias odio, quisquam consequatur amet illum quaerat eligendi libero!</p>
                                </div>
                                <div className='pb-5'>
                                    <h2 className='text-gray-700 text-2xl font-semibold pb-4'>Creemos en la calidad</h2>
                                    <p className='text-base text-slate-900 leading-7 text-justify'>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugit ratione corrupti id nihil cumque, sunt accusamus, inventore temporibus omnis consectetur unde alias odio, quisquam consequatur amet illum quaerat eligendi libero!</p>
                                </div>
                                <div>
                                    <h2 className='text-gray-700 text-2xl font-semibold pb-4'>Creemos en la calidad</h2>
                                    <p className='text-base text-slate-900 leading-7 text-justify'>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugit ratione corrupti id nihil cumque, sunt accusamus, inventore temporibus omnis consectetur unde alias odio, quisquam consequatur amet illum quaerat eligendi libero!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Clientes