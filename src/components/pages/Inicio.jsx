import React from 'react'
import { Link } from 'react-scroll'

const Inicio = () => {
    return (
        <>
            <section id='home' className="bg-slate-50 z-10 pt-28">
                <div className='max-w-7xl mx-auto grid grid-cols-1 md:grid-cols-2 pt-20'>
                    <div className='mx-12 md:mx-12'>
                        <h2 className='pb-5 text-slate-700 text-4xl font-normal text-left'>Somos líderes en soluciones de software</h2>
                        <p className='pt-5 text-slate-900 text-lg text-justify leading-9'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet at enim dicta perspiciatis quo doloribus error laudantium, dignissimos pariatur, molestiae quos asperiores natus esse sed excepturi consectetur recusandae aut!</p>
                        <div className='py-10'>
                            <Link to='solutions' smooth={true} duration={1000}>
                                <button type='button' className='bg-gradient-to-r from-slate-500 to-cyan-600 transition-all duration-500 text-neutral-50 text-md uppercase p-3 px-8 rounded-md cursor-pointer hover:-translate-y-1 hover:shadow-xl hover:shadow-gray-300 hover:transition-all hover:duration-500'>Ir a Soluciones</button>
                            </Link>
                        </div>
                    </div>
                    <div className='md:mx-12 mx-12 md:block hidden'>
                        <img className='' src={`../assets/img/software-development.png`} alt="Imagen de inicio" />
                    </div>
                </div>
            </section>
        </>
    )
}

export default Inicio