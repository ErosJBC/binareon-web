import React from 'react'
import './Button.css'
import { Link } from 'react-router-dom'

const Button = () => {
    return (
        <Link to='iniciar-sesion'>
            <button className='btn'>Iniciar Sesión</button>
        </Link>
    )
}

export default Button